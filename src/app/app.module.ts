import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from './app.component';

import { UsuarioModule } from "./components/usuario/usuario.module";
import { UsuarioRoutingModule } from './components/usuario/usuario.routing.module';
import { HistoricoCombustivelModule } from "./components/historico-combustivel/historico-combustivel.module";
import { HistoricoCombustivelRoutingModule } from "./components/historico-combustivel/historico-combustivel.routing.module";
import { NavbarComponent } from './components/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { ConversorComponent } from './components/conversor/conversor.component';

import { NgxCurrencyModule } from "ngx-currency";

import {registerLocaleData} from '@angular/common';
import br from '@angular/common/locales/br';

registerLocaleData(br, 'pt-BR');

import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        HttpClientModule,
        UsuarioRoutingModule,
        AppRoutingModule,
        UsuarioModule,
        HistoricoCombustivelModule,
        HistoricoCombustivelRoutingModule,
        NgxCurrencyModule,
        NgxPaginationModule
    ],
    declarations: [AppComponent, NavbarComponent, ConversorComponent ],
    bootstrap: [AppComponent]
})
export class AppModule { }