import { Component } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { Router } from '@angular/router';

@Component({ 
    selector: 'usuario-list', 
    templateUrl: 'usuario-list.component.html' 
})
export class UsuarioListComponent {

    pesquisaObj = {
        nome: ""
    };

    rows:any = [];

    constructor(private usuarioService: UsuarioService,
                private router: Router) {
        //this.toastr.success("testando toastr", "TITULO");
    }

    pesquisar() {
        this.usuarioService.pesquisar(this.pesquisaObj.nome).subscribe(res => {
            this.rows = res.body;
        });
    }

    edit(id: number) {
        this.router.navigate(['/usuarios/' + id]);
    }

    remove(id: number) {
        var conf = confirm("Tem certeza que deseja exlcuir o registro?");
        if (conf) {
            this.usuarioService.delete(id).subscribe(res => {
                this.pesquisar();
            });
        }
    }
}