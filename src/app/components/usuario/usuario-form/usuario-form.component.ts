import { Component } from '@angular/core';
import { Usuario } from '../../../domain/usuario';
import { UsuarioService } from '../../../services/usuario.service';

import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';

@Component({ 
    selector: 'usuario-form', 
    templateUrl: 'usuario-form.component.html' 
})
export class UsuarioFormComponent {

    form = this.formBuilder.group({
        nome: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        senha: ['', Validators.required],        
        confSenha: ['', [Validators.required]],
    });

    usuario = new Usuario();

    confSenha: string;

    editing: Boolean = false;

    constructor(private service: UsuarioService,
                private route: ActivatedRoute,
                private router: Router,
                private formBuilder: FormBuilder) {
        let id = this.route.snapshot.params.id;
        if (id) {
            this.form.get('senha').setValidators([]);
            this.form.get('confSenha').setValidators([]);
            this.editing = true;
            this.service.get(id).subscribe(res => {
                this.usuario = Object.assign(this.usuario, res.body);
                this.setForm();
            });
        }
    }

    save() {
        if (this.editing) {
            this.update();
        } else {
            this.setUsuario();
            this.service.save(this.usuario).subscribe(res => {
                this.usuario = Object.assign(this.usuario, res.body);
                this.router.navigate(['/usuarios']);
            });
        }
    }

    update() {
        this.setUsuario();
        this.service.update(this.usuario).subscribe(res => {
            this.usuario = Object.assign(this.usuario, res.body);
            this.router.navigate(['/usuarios']);
        });
    }

    setUsuario() {
        this.usuario.nome = this.form.value.nome;
        this.usuario.email = this.form.value.email;
        this.usuario.senha = this.form.value.senha;
    }

    setForm() {
        this.form.get("nome").setValue(this.usuario.nome);
        this.form.get("email").setValue(this.usuario.email);
        this.form.get("senha").setValue(this.usuario.senha);
    }

}