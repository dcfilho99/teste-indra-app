import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { UsuarioFormComponent } from './usuario-form/usuario-form.component';

const routes: Routes = [
    { path: 'usuarios', component: UsuarioListComponent },
    { path: 'usuarios/new', component: UsuarioFormComponent },
    { path: 'usuarios/:id', component: UsuarioFormComponent }
];

@NgModule({
    exports: [ RouterModule ],
    imports: [ RouterModule.forRoot(routes) ]
  })
export class UsuarioRoutingModule {}