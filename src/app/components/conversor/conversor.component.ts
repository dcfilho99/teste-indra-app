import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../services/historico.comb.serice';
import { HttpEvent, HttpEventType } from '@angular/common/http';

@Component({ 
    selector: 'conversor', 
    templateUrl: 'conversor.component.html' 
})
export class ConversorComponent {

    fileToUpload: File;

    progress = 0;

    constructor(private service: HistoricoCombustivelService) {

    }

    upload() {
        this.service.uploadCsv(this.fileToUpload).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
              case HttpEventType.Sent:
                console.log('Request has been made!');
                break;
              case HttpEventType.ResponseHeader:
                console.log('Response header has been received!');
                break;
              case HttpEventType.UploadProgress:
                console.log(event);
                this.progress = Math.round(event.loaded / event.total * 100);
                console.log(`Uploaded! ${this.progress}%`);
                break;
              case HttpEventType.Response:
                console.log('User successfully created!', event.body);
                setTimeout(() => {
                  this.progress = 0;
                }, 1500);
      
            }
        });
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }
}