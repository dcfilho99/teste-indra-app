import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';

@Component({ 
    selector: 'historico-combustivel-list', 
    templateUrl: 'historico-combustivel-list.component.html' 
})
export class HistoricoCombustivelListComponent {

    page = 1;

    sigla = ""

    rows:any = [];
    totalItens: number;

    tabs = ['pesquisa', 'mediaPreco', 'valores'];
    tabActive = 'pesquisa';

    constructor(private service: HistoricoCombustivelService,
                private router: Router) {

    }

    pesquisar() {
        this.service.pesquisar(this.sigla, this.page-1).subscribe(res => {
            this.rows = res.body;
            this.page = this.rows.pageable.pageNumber;
            this.totalItens = this.rows.totalElements;
            this.rows = this.rows.content;
        });
    }

    nextPage() {
        this.page++;
        this.pesquisar();
    }

    previousPage() {
        this.page--;
        this.pesquisar();
    }

    edit(id: number) {
        this.router.navigate(['/historico-combustiveis/' + id]);
    }

    remove(id: number) {
        var conf = confirm("Tem certeza que deseja exlcuir o registro?");
        if (conf) {
            this.service.delete(id).subscribe(res => {
                this.pesquisar();
            });
        }
    }

    setTabActive(name: string) {
        this.tabActive = name;
    }
}