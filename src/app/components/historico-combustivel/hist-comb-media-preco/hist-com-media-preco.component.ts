import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';

@Component({ 
    selector: 'hist-comb-media-preco', 
    templateUrl: 'hist-comb-media-preco.component.html' 
})
export class HistoricoCombustivelMediaPrecoComponent {

    municipio = ""

    rows:any = [];

    constructor(private service: HistoricoCombustivelService,
                private router: Router) {

    }

    pesquisar() {
        this.service.getMediaPreco(this.municipio).subscribe(res => {
            this.rows = res.body;
        });
    }
}