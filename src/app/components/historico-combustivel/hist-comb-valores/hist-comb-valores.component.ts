import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router } from '@angular/router';

@Component({ 
    selector: 'hist-comb-valores', 
    templateUrl: 'hist-comb-valores.component.html' 
})
export class HistoricoCombustivelValoresComponent {

    municipio = "";
    bandeira = "";

    rowsBandeira:any = [];
    rowsMunicipio:any = [];

    tipoPesquisa = "municipio";

    constructor(private service: HistoricoCombustivelService,
                private router: Router) {

    }

    pesquisarBandeiras() {
        this.service.getValorMedioBandeira(this.bandeira).subscribe(res => {
            this.rowsBandeira = res.body;
        });
    }

    pesquisarMunicipios() {
        this.service.getValorMedioMunicipio(this.municipio).subscribe(res => {
            this.rowsMunicipio = res.body;
        });
    }
}