import { Component, OnInit } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';

@Component({ 
    selector: 'hist-comb-data-coleta', 
    templateUrl: 'hist-comb-data-coleta.component.html' 
})
export class HistoricoCombustivelDataColetaComponent implements OnInit {

    rows:any = [];

    constructor(private service: HistoricoCombustivelService) {

    }

    ngOnInit() {
        this.pesquisar();
    }

    pesquisar() {
        this.service.getAgrupadosDtColeta().subscribe(res => {
            this.rows = res.body;
        });
    }
}