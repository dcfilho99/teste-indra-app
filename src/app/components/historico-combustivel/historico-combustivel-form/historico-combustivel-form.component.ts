import { Component } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({ 
    selector: 'historico-combustivel-form', 
    templateUrl: 'historico-combustivel-form.component.html' 
})
export class HistoricoCombustivelFormComponent {

    form = this.formBuilder.group({
        id: [null, []],
        dtColeta: ['', Validators.required],
        regiaoSigla: ['', [Validators.required]],
        estadoSigla: ['', Validators.required],        
        municipio: ['', [Validators.required]],
        revenda: ['', [Validators.required]],
        cnpjRevenda: ['', [Validators.required]],
        bandeira: ['', [Validators.required]],
        produto: ['', [Validators.required]],
        vlrCompra: ['', [Validators.required]],
        vlrVenda: ['', [Validators.required]],
        unidadeMedida: ['', [Validators.required]],
    });

    historico:any = {
        dtColeta: null
    }

    confSenha: string;

    editing: Boolean = false;

    constructor(private service: HistoricoCombustivelService,
                private route: ActivatedRoute,
                private router: Router,
                private formBuilder: FormBuilder) {
        let id = this.route.snapshot.params.id;
        if (id) {
            
            this.editing = true;
            this.service.get(id).subscribe(res => {
                this.historico = Object.assign(this.historico, res.body);
                //this.historico.dtColeta = new Date(this.historico.dtColeta);
                this.setForm();
                console.log(this.form.value);
            });
        }
    }

    save() {
        if (this.editing) {
            this.update();
        } else {
            this.setHistoricoComsutivel();
            this.service.save(this.historico).subscribe(res => {
                this.historico = Object.assign(this.historico, res.body);
                this.router.navigate(['/historico-combustiveis']);
            });
        }
    }

    update() {
        this.setHistoricoComsutivel();
        this.service.update(this.historico).subscribe(res => {
            this.historico = Object.assign(this.historico, res.body);
            this.router.navigate(['/historico-combustiveis']);
        });
    }

    setHistoricoComsutivel() {
         this.historico = Object.assign(this.historico, this.form.value);
    }

    setForm() {
        console.log(this.historico.dtColeta.toString());
        console.log(new Date(this.historico.dtColeta.toString()).toUTCString());
        this.form.setValue(this.historico);
    }
}