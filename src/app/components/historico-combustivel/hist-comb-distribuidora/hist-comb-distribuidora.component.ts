import { Component, OnInit } from '@angular/core';
import { HistoricoCombustivelService } from '../../../services/historico.comb.serice';

@Component({ 
    selector: 'hist-comb-distribuidora', 
    templateUrl: 'hist-comb-distribuidora.component.html' 
})
export class HistoricoCombustivelDistribuidoraComponent implements OnInit {

    rows:any = [];

    constructor(private service: HistoricoCombustivelService) {

    }

    ngOnInit() {
        this.pesquisar();
    }

    pesquisar() {
        this.service.getAgrupadosDistribuidora().subscribe(res => {
            this.rows = res.body;
        });
    }
}