import { Injectable } from '@angular/core';
import { ApiService } from './api';

@Injectable({
  providedIn: 'root'
})
export class HistoricoCombustivelService {

  endpoint = "/v1/historico-combustiveis";

  constructor(private api: ApiService) { 

  }

  pesquisar(regiaoSigla: string, page: number) {
    var url = this.endpoint + "/pesquisa?regiaoSigla=" + regiaoSigla + "&page=" + page;
    return this.api.get(url);
  }

  save(hist: Object) {
    return this.api.post(this.endpoint, hist);
  }

  get(id: number) {
    var url = this.endpoint + "/" + id;
    return this.api.get(url);
  }

  getMediaPreco(municipio: string) {
    var url = this.endpoint + "/pesquisa/media-preco?municipio=" + municipio;
    return this.api.get(url);
  }

  getValorMedioMunicipio(municipio: string) {
    var url = this.endpoint + "/pesquisa/valores-municipio?municipio=" + municipio;
    return this.api.get(url);
  }

  getValorMedioBandeira(bandeira: string) {
    var url = this.endpoint + "/pesquisa/valores-bandeira?bandeira=" + bandeira;
    return this.api.get(url);
  }

  getAgrupadosDistribuidora() {
    return this.api.get(this.endpoint + "/group-by/revenda");
  }

  getAgrupadosDtColeta() {
    return this.api.get(this.endpoint + "/group-by/data-coleta");
  }

  update(hist: Object) {
    return this.api.put(this.endpoint, hist);
  }

  delete(id: number) {
    var url = this.endpoint + "/" + id;
    return this.api.delete(url);
  }

  uploadCsv(file: File) {
    const endpoint = this.endpoint + '/upload-csv';
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.api
      .postFile(endpoint, formData);
  }
}