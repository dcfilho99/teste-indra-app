import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    host: any = environment.api;

    httpOptions = {
        headers: {
          'Content-Type': 'application/json'
        }
      }  

    constructor(private httpClient: HttpClient) {
        
    }
    
    public post(url: string, data: Object) {
        var request = this.httpClient.post(this.host + url, data, {headers: this.httpOptions.headers, observe: 'response'});
        return request;
    }

    public postFile(url: string, data: FormData) {
        var request = this.httpClient.post(this.host + url, data, {
            reportProgress: true,
            observe: 'events'
        });
        return request;
    }

    public get(url: string) {
        var request = this.httpClient.get(this.host + url, {headers: this.httpOptions.headers, observe: 'response'});
        return request;
    }

    public put(url: string, data: Object) {
        var request = this.httpClient.put(this.host + url, data, {headers: this.httpOptions.headers, observe: 'response'});
        return request;
    }

    public delete(url: string) {
        var request = this.httpClient.delete(this.host + url, {headers: this.httpOptions.headers, observe: 'response'});
        return request;
    }

    public errorHandler(error) {
        alert(error.mensagem);
    } 
}